package testData;

public class TestData {
    String label;
    String quantity;
    String color;
    String productName;

    public String getLabel() {
        return label = "Тест";
    }

    public String getQuantity() {
        return quantity = "5";
    }

    public String getColor() {
        return color = "Red";
    }

    public String getProductName() {
        return productName = "Перекус для собачки";
    }
}