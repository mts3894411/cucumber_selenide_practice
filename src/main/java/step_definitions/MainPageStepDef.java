package step_definitions;

import io.cucumber.java.bg.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPageStepDef {
    public static final By BLOG_BUTTON = By
            .xpath("//ul[@class='uk-navbar-nav']//a[@href='/index.php/news']");
    public static final By SHOP_BUTTON = By
            .xpath("//ul[@class='uk-navbar-nav']//a[@href='/index.php/magazin']");

    @И("^открыть страницу$")
    public void openMainPage() {
        open("https://qahacking.guru/");
    }

    @И("^перейти в раздел Блог Джесси$")
    public void goToBlogJesse() {
        $(BLOG_BUTTON).click();
    }

    @И("^перейти в магазин$")
    public void goToShop() {
        $(SHOP_BUTTON).doubleClick();
    }
}