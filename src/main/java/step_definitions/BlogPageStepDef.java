package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.bg.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BlogPageStepDef {
    public static final By BLOG_NAME = By.xpath("//h1");

    @И("^проверить совпадение названия открывшейся статьи и \"Давно просил домашнее животное\"$")
    public void checkName() {
        $(BLOG_NAME).shouldHave(Condition.text("Давно просил домашнее животное"));
    }
}