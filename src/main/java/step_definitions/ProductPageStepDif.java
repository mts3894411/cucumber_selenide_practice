package step_definitions;

import dto.OrderInfo;
import dto.SetupYml;
import io.cucumber.java.bg.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ProductPageStepDif {

    public static final OrderInfo orderInfo = SetupYml.setup();
    public static final By DROPDOWN_LIST_COLOR = By.cssSelector("select.inputbox");
    public static final By RED_COLOR_BUTTON = By.cssSelector("option[value='2']");
    public static final By LABEL_FIELD = By.cssSelector("input[name='freeattribut[1]']");
    public static final By QUANTITY_FIELD = By.cssSelector("input[name='quantity']");
    public static final By BASKET_BUTTON = By.cssSelector("input[type='submit']");

    @И("^выбрать в выпадающем списке \"Color\" цвет \"Red\"$")
    public void selectColor() {
        $(DROPDOWN_LIST_COLOR).click();
        $(RED_COLOR_BUTTON).click();
    }

    @И("заполнить поле \"Label\"")
    public void fillLabelField() {
        String label = orderInfo.getLabel();
        $(LABEL_FIELD).sendKeys(label);
    }

    @И("заполнить поле\"Количество\"")
    public void fillQuantityField() {
        String quantity = orderInfo.getQuantity();
        $(QUANTITY_FIELD).clear();
        $(QUANTITY_FIELD).sendKeys(quantity);
    }

    @И("нажать на кнопку \"В корзину\"")
    public void clickBasketButton() {
        $(BASKET_BUTTON).click();
    }
}