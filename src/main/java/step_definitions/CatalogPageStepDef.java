package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import dto.OrderInfo;
import dto.SetupYml;
import io.cucumber.java.bg.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class CatalogPageStepDef {

    public static final OrderInfo orderInfo = SetupYml.setup();
    public static final By PRODUCT_NAME = By.cssSelector("div.name>a");

    @И("^выбрать продукт \"(.*)\"$")
    public void selectProduct(String productName) {
        SelenideElement product = $$(PRODUCT_NAME)
                .findBy(Condition.text(productName));
        product.click();
    }
}