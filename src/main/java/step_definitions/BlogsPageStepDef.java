package step_definitions;

import io.cucumber.java.bg.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BlogsPageStepDef {
    public static final By NAME_BLOG = By.xpath("//a[@class='uk-link-reset']");

    @И("^нажать на широкую картинку во весь экран с названием \"Давно просил домашнее животное\"$")
    public void clickBlogName() {
        $(NAME_BLOG).click();
    }
}