package step_definitions;

import com.codeborne.selenide.Condition;
import dto.OrderInfo;
import dto.SetupYml;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static org.junit.Assert.assertEquals;

public class BasketPageStepDef {

    public static final OrderInfo orderInfo = SetupYml.setup();
    public static final By NAME = By.xpath("//a[contains(text(),'Перекус для собачки')]");
    public static final By COLOR = By.cssSelector("div.list_attribute");
    public static final By LABEL = By.cssSelector("div.list_free_attribute");
    public static final By PRICE_PER_UNIT = By.cssSelector("td.single_price>div.data");
    public static final By QUANTITY = By.cssSelector("input[name='quantity[0]']");
    public static final By SUM_IN_TABLE = By.cssSelector("td.total_price>div.data");
    public static final By SUM_IN_RESULT = By.cssSelector("tr.total>td.value");

    @И("^проверить совпадение поля \"Label\"с введенными ранее значениями$")
    public void checkLabel() {
        String label = orderInfo.getLabel();
        $(LABEL).shouldBe(Condition.text(label));
    }

    @И("^проверить совпадение названия с тестовыми данными$")
    public void checkProductName() {
        String productName = orderInfo.getProductName();
        $(NAME).shouldBe(Condition.text(productName));
    }

    @И("^проверить совпадение поля \"Color\"с тестовыми данными$")
    public void checkColor() {
        String color = orderInfo.getColor();
        $(COLOR).shouldBe(Condition.text(color));
    }

    @И("^проверить совпадение поля \"Цена за единицу\" с введенными ранее значениями$")
    public void checkPricePerUnitField() {
        $(PRICE_PER_UNIT).shouldHave();
    }

    @И("^проверить совпадение поля \"Количество\" с введенными тестовыми данными$")
    public void checkQuantityField() {
        String quantity = orderInfo.getQuantity();
        String quantityInTable = $(QUANTITY).getValue();
        assertEquals(quantity, quantityInTable);
    }

    @И("^проверить совпадение поля \"Сумма\" в таблице и в итоговом результате$")
    public void checkSumField() {
        String sumInResult = $(SUM_IN_RESULT).innerText();
        $(SUM_IN_TABLE).shouldBe(Condition.text(sumInResult));
    }
}