package dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class OrderInfo {
    @JsonAlias("Лейбл")
    String label;
    @JsonAlias("Количество единиц товара")
    String quantity;
    @JsonAlias("Цвет")
    String color;
    @JsonAlias("Название товара")
    String productName;
}