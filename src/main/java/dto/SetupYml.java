package dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;

public class SetupYml {
    public static OrderInfo setup(){
        File file = new File("src/test/java/yml/testData.yml");
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(file, OrderInfo.class);
        } catch (IOException e) {
            throw new RuntimeException("Не читается файл",e);
        }
    }
}